import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)
np.set_printoptions(linewidth=np.inf)

class DatasetProvider():
    def __init__(self, path):
        csv = pd.read_csv(path)
        self.data = []
        self.labels = ["gender", "age", "hypertension", "ever_married", "Residence_type", "avg_glucose_level", "bmi", "heart_disease"]

        self.data.append(np.array(csv['gender'] == "Male", dtype='float32'))
        self.data.append(np.array(csv['age'], dtype='float32'))
        self.data.append(np.array(csv['hypertension'], dtype='float32'))
        self.data.append(np.array(csv['ever_married'] == "Yes", dtype='float32'))
        self.data.append(np.array(csv['Residence_type'] == "Urban", dtype='float32'))
        self.data.append(np.array(csv['avg_glucose_level'], dtype='float32'))
        bmi = np.nan_to_num(np.array(csv['bmi'], dtype='float32'))
        mean_clear_bmi = bmi[np.where(bmi != 0)].mean()
        bmi[np.where(bmi == 0)] = mean_clear_bmi
        self.data.append(bmi) #Body mass index
        self.data.append(np.array(csv['heart_disease'], dtype='int32')*2 - 1)
        self.data = np.array(self.data)
    def printInfo(self):
        '''
        data = self.data.swapaxes(0, 1)
        mean = data.mean(axis=0, keepdims=True)
        std = data.std(axis=0, keepdims=True)
        data = (data - mean) / std
        data = data.swapaxes(0,1)
        '''
        data = self.data
        for i in range(len(self.data)):
            print(f"{self.labels[i]} stats")
            print(f"\tMean: {data[i].mean()}")
            print(f"\tVar: {data[i].std()}")
            print("--------------------")

        print(self.labels)
        print("Corellation matrix")
        print(np.corrcoef(data))
        print("--------------------")
        plt.matshow(np.corrcoef(data), cmap='cool')
        plt.show()

        #fig, ax = plt.subplots((len(self.data)))
        for i in range(len(self.data)):
            #ax[i].scatter(x=self.data[i], y=self.data[-1])

            #healty
            #plt.scatter(x=data[i], y=data[-1])
            plt.hist(data[i])
            plt.xlabel(self.labels[i])

            plt.show()



    def clear_data(self, train_sllit = 0.75):
        data = self.data.swapaxes(0, 1)
        mean = data.mean(axis=0, keepdims=True)
        std = data.std(axis=0, keepdims=True)
        data = (data - mean) / std

        return (
            data[:int(train_sllit * data.shape[0]), :-1],
            data[:int(train_sllit * data.shape[0]), -1],

            data[int(train_sllit * data.shape[0]):, :-1],
            data[int(train_sllit * data.shape[0]):, -1]
        )
    def PCA(self):
        data = self.data.swapaxes(0, 1)
        mean = data.mean(axis=0, keepdims=True)
        std = data.std(axis=0, keepdims=True)
        data = (data - mean) / std



        data = data.swapaxes(0, 1)
        cov = np.cov(data)
        _, vecs = np.linalg.eig(cov)
        for i in range(vecs.shape[1]):
            v = -vecs[:, i]
            newdata = np.vstack([data, np.dot(v, data)])
            print(f"Corellation matrix for v= {v}")
            print(np.corrcoef(newdata))
            print("--------------------")
            plt.matshow(np.corrcoef(newdata), cmap='cool')
            plt.show()

    def generate_batches_labels(self, train_sllit = 0.9):
        (train_x, train_y), (test_x, test_y) = self.generate_batches_logits(train_sllit=train_sllit)
        return (train_x, self.log2lab(train_y)),(test_x, self.log2lab(test_y))

    def log2lab(self, logits):
        labels = [(1,0), (0,1)]
        return np.array([labels[int(logit)] for logit in logits])
    def lab2log(self, labels):
        return np.array([np.argmax(label) for label in labels])

    def generate_batches_logits(self, train_sllit = 0.9):
        data = self.data.swapaxes(0,1)
        #Normalize each feature independently
        mean = data[:,:-1].mean(axis=0, keepdims=True)
        std = data[:,:-1].std(axis=0, keepdims=True)
        data[:,:-1] = (data[:,:-1] - mean) / std



        #balance classes
        pos = data[np.where(np.array(data[:,-1]) > 0)]
        neg = data[np.where(np.array(data[:,-1]) < 0)][:pos.shape[0]]
        data_train = np.vstack([neg[:int(neg.shape[0] * train_sllit)], pos[:int(pos.shape[0] * train_sllit)]])
        data_test = np.vstack([neg[int(neg.shape[0] * train_sllit):], pos[int(pos.shape[0] * train_sllit):]])


        return (data_train[:,:-1], (data_train[:, -1]+1)/2), (data_test[:,:-1], (data_test[:, -1]+1)/2)