import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.utils import plot_model
import numpy as np
import os
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'

class BinaryClassifier():
    def __init__(self,
                 lr=1e-3,
                 features=7
                 ):
        self.optimizer = keras.optimizers.Adam(learning_rate=lr)
        self.classifier = self._build_predictor(features)
        self.writer = tf.summary.create_file_writer("./tensorboard/")
        self.trainstep = 0
        print("Total amount of predictor trainable parameters: {}\n"
            .format(
            np.sum([np.prod(v.get_shape().as_list()) for v in self.classifier.trainable_variables]),
        ))
        plot_model(self.classifier, to_file='./classifier.png', show_shapes=True, show_layer_names=True)



    def _loss(self, result, target):
        bce = tf.losses.CategoricalCrossentropy(from_logits=True)
        return bce(result, target)

    def train_step(self, input, target):
        input = tf.cast(input, dtype='float32')
        with tf.GradientTape() as grad_tape:
            result = tf.cast(self.classifier(input), dtype='float32')
            target = tf.cast(target, dtype='float32')
            l = self._loss(result, target)
            with (self.writer.as_default()):
                tf.summary.scalar("Loss", l,
                                  step=self.trainstep)
                tf.summary.histogram("result", result, step=self.trainstep)
                self.writer.flush()
            grads = grad_tape.gradient(l, self.classifier.trainable_variables)
            self.optimizer.apply_gradients(zip(grads, self.classifier.trainable_variables))
            self.trainstep+=1
            return l

    def save(self):
        self.classifier.save_weights("./save/")
    def load(self):
        self.classifier.load_weights("./save/")

    def _build_predictor(self, features_count):
        i1 = layers.Input(shape=[None, features_count])
        with tf.device("/gpu:0"):
            o = layers.Dense(2, use_bias=True,
                             kernel_initializer='he_normal',
                             kernel_regularizer=tf.keras.regularizers.L2(0.01),
                             activation="softmax")(i1)
            return tf.keras.Model(inputs=i1, outputs=o)
