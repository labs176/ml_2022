from dataset.dataset_provider import DatasetProvider
from model.BinaryClassifier import BinaryClassifier
from sklearn.neighbors import NearestNeighbors
from sklearn.ensemble import RandomForestClassifier
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

provider = DatasetProvider("./dataset/healthcare-dataset-stroke-data.csv")
#provider.printInfo()
#provider.PCA()



(train_x, train_y), (test_x, test_y) = provider.generate_batches_labels(train_sllit=0.9)


classifier = BinaryClassifier(
    lr=1e-3,
    features=train_x.shape[-1]
)

#classifier.load()


for epoch in range(200):
    loss = classifier.train_step(np.array([train_x]), np.array([train_y], dtype="float32")) / train_x.shape[0]
    print(f"[{epoch}] Loss: {loss}")
    classifier.save()


r = classifier.classifier(np.array([test_x]))[0]
r = provider.lab2log(r)

test_y = provider.lab2log(test_y)
confmatrix = tf.math.confusion_matrix(
    test_y,
    r,
    num_classes=2,
    dtype=tf.dtypes.float32,
)
print(confmatrix)
plt.matshow(confmatrix)
plt.show()

accuracy = len(np.where(test_y == r)[0])/len(test_y)*100
print(accuracy)