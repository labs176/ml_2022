from dataset.dataset_provider import DatasetProvider
from sklearn.ensemble import RandomForestClassifier
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


provider = DatasetProvider("./dataset/healthcare-dataset-stroke-data.csv")
(train_x, train_y), (test_x, test_y) = provider.generate_batches_logits(train_sllit=0.9)

clf = RandomForestClassifier(n_estimators=100)
clf = clf.fit(train_x, train_y)
r = clf.predict(test_x)


accuracy = len(np.where(test_y == r)[0])/len(test_y)*100

print(accuracy)

confmatrix = tf.math.confusion_matrix(
    test_y,
    r,
    num_classes=2,
    dtype=tf.dtypes.float32,
)
print(confmatrix)
plt.matshow(confmatrix)
plt.show()